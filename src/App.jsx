import {useState} from 'react'
import Controllers from './components/Controllers/Controllers'
import ModalBase from './components/Modal/ModalBesic'
import ModalImage from './components/ModalImage/ModalImage'
import WrapperApp from './components/WrapperApp/WrapperApp'
import './App.css'

const App = (props) =>{
  const [isModal,setIsModal] = useState(false)
  const [isModalImage,setIsModalImage] = useState(false)

  const handleModal = () => setIsModal(!isModal)
  const handleModalImage = () => setIsModalImage(!isModalImage)

  return (
    <>
      <WrapperApp>
        <Controllers 
          handleModal={handleModal}
          handleModalImage={handleModalImage}
        />
        <ModalBase
            isOpen={isModal}
            title="Add Product NAME"
            desc="Go ADD Product"
            handleClose={handleModal}
        />
        <ModalImage
            isOpen={isModalImage}
            title="Product delete!"
            desc="Some text"
            handleClose={handleModalImage}
        />
      </WrapperApp>
    </>
  )
}

export default App
