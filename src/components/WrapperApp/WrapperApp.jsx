import cx from "classnames"
import PropTypes from 'prop-types'


// .widget-container.neutral
// .widget-container.cold
// .widget-container.hot

// const backgoundColor = () => {
//     const currentClass = ["widget-container"]
//     if(temperature<10){
//         currentClass.push("cold")
//     } else if(temperature>20){
//         currentClass.push("hot")
//     } else {
//         currentClass.push("neutral")
//     }
//     return currentClass.join(" ")
// }

// return (
//     <div className={backgoundColor()}>
//         {children}
//     </div>
// )

const WrapperApp = ({children})=>{
    return(
        
           <div className="widget-container">{children}</div> 
        
        
    )
}

WrapperApp.propTypes = {
    children: PropTypes.any
}

export default WrapperApp