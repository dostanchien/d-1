import PropTypes from 'prop-types'
import ModalWrapper from "../Modal/ModalWrapper"
import Modal from "../Modal/Modal"
import ModalHeader from "../Modal/ModalHeader"
import ModalBody from "../Modal/ModalBody"
import ModalFooter from "../Modal/ModalFooter"
import ModalClose from "../Modal/ModalClose"

const ModalImage = ({title, desc, handleOk, handleClose, isOpen}) =>{
    const handleOutside = (event) => {
        if(!event.target.closest(".modal")){
                handleClose()
        }
    }
    return(
        <ModalWrapper  click={handleOutside} isOpen={isOpen}>
            <Modal>
                <ModalClose click={handleClose}/>
                <ModalHeader>
                    <figure>              
                        <img className="card-img-top" src="https://media.md-fashion.com.ua/images/60/80/c98449f910be2e5033b2729b9db9.jpg" alt="" />
                    </figure>
                </ModalHeader>
                <ModalBody>
                    <div>{title}</div>
                    <p>{desc}</p>
                </ModalBody>
                <ModalFooter textFirst="NO, CANCEL" textSecondary="YES, DELETE" clickFirst={handleOk} clickSecondary={handleClose}/>
            </Modal>
        </ModalWrapper>
    )
}
ModalImage.propTypes = {
    title: PropTypes.string,
    desk: PropTypes.string,
    handleOk: PropTypes.string,
    handleClose: PropTypes.func,
    isOpen: PropTypes.bool
}

export default ModalImage