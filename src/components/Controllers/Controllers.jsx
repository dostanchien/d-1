import Button from "../Button/Button"
import PropTypes from 'prop-types'
const Controllers = ({handleModal,handleModalImage}) => {

  return (
    <div className="widget-controllers">
        <div className="button-container">
            <Button click={handleModal} >Open first modal</Button>
            <Button click={handleModalImage}>Open second modal</Button>
        </div>
    </div>
  )
}

Controllers.propTypes = {
  handleModal: PropTypes.func,
  handleModalImage: PropTypes.func
}

export default Controllers