import cx from "classnames"
import PropTypes from 'prop-types'
import './Button.scss';


// const ButtonSt = styled.button`
//     background: ${(props) => props.ver === "promery"?"#000":"red"};

//     &.btn-red {
//       background: red
//     }
//   `
  // 1)func = <button class="sjfhdfs">
  // 1)css selector = <button class="sjfhdfs btn-red">



const Button = (props) => {
  const {type, classNames, boxView, underlineView, children, click, ...restProps} = props
  ///...restProps == {.......}
  return (
    <button onClick={click} className={cx("button", classNames, {_box:boxView}, {"_box-underline":underlineView})} type={type} {...restProps}>{children}</button>
  )
}
Button.defaultProps = {
  type: "button",
  click: () => {}
}

Button.propTypes = {
  type: PropTypes.string,
  classNames: PropTypes.string, 
  boxView: PropTypes.bool,
  underlineView: PropTypes.bool,
  children: PropTypes.any,
  click: PropTypes.func
}

export default Button