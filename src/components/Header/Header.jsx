const Header = () => { // props => {}  ===== если просов меньше 3
  // const {isMenu,isMenu,logoisMenu,logoisMenu,logoisMenu} = props; ======= если пропсов > 3
  // const {
    // isMenu,
    // isMenu,
    // logoisMenu,
    // logoisMenu,
    // logoisMenu,
    // logoisMenu,
    // logoisMenu,
    // logoisMenu,
    // logo
    // } = props;  ======= если пропсов > 5

    // const {
    // isMenu,
    // isMenu,
    // logoisMenu,
    // logoisMenu,
    // logoisMenu,
    // logoisMenu,
    // logoisMenu,
    // logoisMenu,
    // logo
    // } = data;
  // console.log('props: logo', logo);
  // console.log('props: isMenu', isMenu);

  // const data = [{name:"test1"},{name:"test2"},{name:"test3"},{name:"test4"},{name:"test5"},{name:"test6"},{name:"test7"}]

  // const itemData = data.map(({name}, index) => (<span key={Date.now()}>{name}</span>))
  const daysArr = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const monthsArr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  const date = new Date()
  const days = daysArr[date.getDay()] //0-6
  const day = date.getDate() //0-30
  const month = monthsArr[date.getMonth()] // 0-11
  // console.log('days',days);
  return (
      <div className="header-wrapper">
        <div className="header">
          <div className="current-date">
              <p className="day">{days}</p>
              <p className="data">{day} {month}</p>
          </div>
        </div>
    </div>
  )
}

export default Header